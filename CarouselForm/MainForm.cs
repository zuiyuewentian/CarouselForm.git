﻿using CarouselForm.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CarouselForm
{
    public partial class MainForm : Form
    {
        MainFormController mainFormController ;

        public MainForm()
        {
            InitializeComponent();

            mainFormController = new MainFormController(panel_Main);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            mainFormController.Init(5, 3, panel_Main.Width, panel_Main.Height);
        }

        /// <summary>
        /// 右
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            mainFormController.SlideToRight();
        }

        /// <summary>
        /// 左
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            mainFormController.SlideToLeft();
        }

        /// <summary>
        /// 上
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            mainFormController.SlideToUp();
        }

        /// <summary>
        /// 下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            mainFormController.SlideToDown();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            mainFormController.SlideToHome();
        }
    }
}
