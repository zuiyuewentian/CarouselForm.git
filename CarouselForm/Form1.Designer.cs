﻿namespace CarouselForm
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel_Body = new System.Windows.Forms.Panel();
            this.pic_right = new System.Windows.Forms.PictureBox();
            this.pic_left = new System.Windows.Forms.PictureBox();
            this.timer_change = new System.Windows.Forms.Timer(this.components);
            this.panel_Body.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_left)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_Body
            // 
            this.panel_Body.BackColor = System.Drawing.Color.White;
            this.panel_Body.Controls.Add(this.pic_left);
            this.panel_Body.Controls.Add(this.pic_right);
            this.panel_Body.Location = new System.Drawing.Point(126, 0);
            this.panel_Body.Name = "panel_Body";
            this.panel_Body.Size = new System.Drawing.Size(648, 449);
            this.panel_Body.TabIndex = 1;
            // 
            // pic_right
            // 
            this.pic_right.Image = global::CarouselForm.Resource.方向圆_向右;
            this.pic_right.Location = new System.Drawing.Point(569, 200);
            this.pic_right.Name = "pic_right";
            this.pic_right.Size = new System.Drawing.Size(50, 50);
            this.pic_right.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_right.TabIndex = 0;
            this.pic_right.TabStop = false;
            this.pic_right.Click += new System.EventHandler(this.pic_right_Click);
            // 
            // pic_left
            // 
            this.pic_left.Image = global::CarouselForm.Resource.方向圆_向左;
            this.pic_left.Location = new System.Drawing.Point(30, 200);
            this.pic_left.Name = "pic_left";
            this.pic_left.Size = new System.Drawing.Size(50, 50);
            this.pic_left.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_left.TabIndex = 1;
            this.pic_left.TabStop = false;
            this.pic_left.Click += new System.EventHandler(this.pic_left_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 449);
            this.Controls.Add(this.panel_Body);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "幻灯片";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_Body.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_left)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Body;
        private System.Windows.Forms.PictureBox pic_left;
        private System.Windows.Forms.PictureBox pic_right;
        private System.Windows.Forms.Timer timer_change;
    }
}

