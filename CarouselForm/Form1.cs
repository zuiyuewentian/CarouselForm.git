﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CarouselForm
{
    public partial class Form1 : Form
    {
        int lastLength = 0;
        int stepLength = 0;
        bool direction = true;

        Panel Panel1 = new Panel();
        Panel Panel2 = new Panel();
        Panel Panel3 = new Panel();

        int CurrentIndex = 0;

        public Form1()
        {
            InitializeComponent();

            timer_change.Interval = 10;
            timer_change.Tick += Timer_change_Tick;
            stepLength = panel_Body.Width / 20;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            Panel1.BackColor = Color.FromArgb(211, 220, 230);
            Panel1.Width = panel_Body.Width;
            Panel1.Height = panel_Body.Height - 100;
            Panel1.Location = new Point(0, 50);

            Panel2.BackColor = Color.FromArgb(153, 169, 191);
            Panel2.Width = panel_Body.Width;
            Panel2.Height = panel_Body.Height - 100;
            Panel2.Location = new Point(panel_Body.Width, 50);

            Panel3.BackColor = Color.FromArgb(211, 220, 230);
            Panel3.Width = panel_Body.Width;
            Panel3.Height = panel_Body.Height - 100;
            Panel3.Location = new Point(panel_Body.Width * 2, 50);

            panel_Body.Controls.Add(Panel1);
            panel_Body.Controls.Add(Panel2);
            panel_Body.Controls.Add(Panel3);

            pic_left.BackColor = Panel1.BackColor;
            pic_right.BackColor = Panel1.BackColor;
        }

        private void Timer_change_Tick(object sender, EventArgs e)
        {
            if (direction)
            {
                if (lastLength + stepLength > panel_Body.Width)
                {
                    int tempStepLength = panel_Body.Width - lastLength;

                    int p1X = Panel1.Location.X - tempStepLength;
                    int p2X = Panel2.Location.X - tempStepLength;
                    int p3X = Panel3.Location.X - tempStepLength;

                    Panel1.Location = new Point(p1X, Panel1.Location.Y);
                    Panel2.Location = new Point(p2X, Panel2.Location.Y);
                    Panel3.Location = new Point(p3X, Panel3.Location.Y);

                    timer_change.Stop();
                }
                else
                {
                    int p1X = Panel1.Location.X - stepLength;
                    int p2X = Panel2.Location.X - stepLength;
                    int p3X = Panel3.Location.X - stepLength;

                    Panel1.Location = new Point(p1X, Panel1.Location.Y);
                    Panel2.Location = new Point(p2X, Panel2.Location.Y);
                    Panel3.Location = new Point(p3X, Panel3.Location.Y);

                    lastLength += stepLength;
                }
            }
            else
            {
                if (lastLength + stepLength > panel_Body.Width)
                {
                    int tempStepLength = panel_Body.Width - lastLength;

                    int p1X = Panel1.Location.X + tempStepLength;
                    int p2X = Panel2.Location.X + tempStepLength;
                    int p3X = Panel3.Location.X + tempStepLength;

                    Panel1.Location = new Point(p1X, Panel1.Location.Y);
                    Panel2.Location = new Point(p2X, Panel2.Location.Y);
                    Panel3.Location = new Point(p3X, Panel3.Location.Y);

                    timer_change.Stop();
                }
                else
                {
                    int p1X = Panel1.Location.X + stepLength;
                    int p2X = Panel2.Location.X + stepLength;
                    int p3X = Panel3.Location.X + stepLength;

                    Panel1.Location = new Point(p1X, Panel1.Location.Y);
                    Panel2.Location = new Point(p2X, Panel2.Location.Y);
                    Panel3.Location = new Point(p3X, Panel3.Location.Y);

                    lastLength += stepLength;
                }
            }


        }

        private void pic_left_Click(object sender, EventArgs e)
        {
            direction = false;
            lastLength = 0;
            timer_change.Start();
            CurrentIndex--;
            SetBackColor();
        }

        private void pic_right_Click(object sender, EventArgs e)
        {
            direction = true;
            lastLength = 0;
            timer_change.Start();
            CurrentIndex++;
            SetBackColor();
        }

        private void SetBackColor()
        {
            if (CurrentIndex == 0)
            {
                pic_left.BackColor = Panel1.BackColor;
                pic_right.BackColor = Panel1.BackColor;
            }
            if (CurrentIndex == 1)
            {
                pic_left.BackColor = Panel2.BackColor;
                pic_right.BackColor = Panel2.BackColor;
            }
            if (CurrentIndex == 2)
            {
                pic_left.BackColor = Panel3.BackColor;
                pic_right.BackColor = Panel3.BackColor;
            }
        }
    }
}
