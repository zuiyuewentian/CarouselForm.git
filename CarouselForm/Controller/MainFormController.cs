﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CarouselForm.Controller
{
    public class MainFormController
    {
        /// <summary>
        /// panel
        /// </summary>
        private List<List<Control>> mainPanels = null;

        /// <summary>
        /// 当前的panel行
        /// </summary>
        private int CurrentPanelIndexRow = 0;

        /// <summary>
        /// 当前的panel列
        /// </summary>
        private int CurrentPanelIndexCol = 0;

        /// <summary>
        /// 方向 0，左，1，上，2，右，3，下
        /// </summary>
        private int Direction = 0;

        /// <summary>
        /// 最后走的长度
        /// </summary>
        private int lastLength = 0;

        /// <summary>
        /// 步长
        /// </summary>
        private int stepLength = 0;

        /// <summary>
        /// 定时器
        /// </summary>
        private Timer timer_change = null;

        /// <summary>
        /// 父panel
        /// </summary>
        private Panel panel_Main = null;

        /// <summary>
        /// 点击锁
        /// </summary>
        private bool Click_Lock = false;

        Color color1 = Color.FromArgb(211, 220, 230);
        Color color2 = Color.FromArgb(153, 169, 191);

        public MainFormController(Panel p_Main)
        {
            panel_Main = p_Main;
            timer_change = new Timer();
            timer_change.Interval = 10;
            timer_change.Tick += Timer_change_Tick;
            stepLength = panel_Main.Width / 20;
        }

        /// <summary>
        /// 向左滑动
        /// </summary>
        public void SlideToLeft()
        {
            if (Click_Lock)
                return;
            Click_Lock = true;
            Direction = 0;
            lastLength = 0;
            timer_change.Start();
        }

        /// <summary>
        /// 向右滑动
        /// </summary>
        public void SlideToRight()
        {
            if (Click_Lock)
                return;
            Click_Lock = true;
            Direction = 2;
            lastLength = 0;
            timer_change.Start();
        }

        /// <summary>
        ///  向上滑动
        /// </summary>
        public void SlideToUp()
        {
            if (Click_Lock)
                return;
            Click_Lock = true;
            Direction = 1;
            lastLength = 0;
            timer_change.Start();
        }

        /// <summary>
        /// 向下滑动
        /// </summary>
        public void SlideToDown()
        {
            if (Click_Lock)
                return;
            Click_Lock = true;
            Direction = 3;
            lastLength = 0;
            timer_change.Start();
        }

        /// <summary>
        /// 回到首页
        /// </summary>
        public void SlideToHome()
        {
            if (Click_Lock)
                return;
            Click_Lock = true;
            CurrentPanelIndexRow = 0;
            CurrentPanelIndexCol = 0;
            ReSetPanelLocation(panel_Main.Width, panel_Main.Height);
            Click_Lock = false ;
        }

        /// <summary>
        /// 初始化行列
        /// </summary>
        /// <param name="col">列</param>
        /// <param name="row">行</param>
        /// <param name="width">宽度</param>
        /// <param name="height">高度</param>
        /// <param name="firstX">初始位置X，默认0</param>
        /// <param name="firstY">初始位置Y，默认0</param>
        public void Init(int col, int row,
                               int width, int height,
                               int firstX = 0, int firstY = 0)
        {
            panel_Main.Controls.Clear();
            mainPanels = new List<List<Control>>();
            for (int i = 0; i < row; i++)
            {
                List<Control> panels = new List<Control>();
                for (int j = 0; j < col; j++)
                {
                    Panel panel = new Panel();
                    panel.Name = "Panel-" + i + "-" + j;
                    panel.Width = width;
                    panel.Height = height;
                    if ((j + i * col) % 2 == 0)
                        panel.BackColor = color1;
                    else
                        panel.BackColor = color2;

                    int lx = firstX + j * width;
                    int ly = firstY + i * height;
                    panel.Location = new Point(lx, ly);

                    Label label = new Label();
                    label.Text = "Panel-" + i + "-" + j;
                    label.Location = new Point(50, 50);
                    panel.Controls.Add(label);

                    panel_Main.Controls.Add(panel);
                    panels.Add(panel);
                }
                mainPanels.Add(panels);
            }
            CurrentPanelIndexRow = 0;
            CurrentPanelIndexCol = 0;
        }

        /// <summary>
        /// 重置所有panel初始位置
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="firstX"></param>
        /// <param name="firstY"></param>
        private void ReSetPanelLocation(int width, int height, int firstX = 0, int firstY = 0)
        {
            for (int i = 0; i < mainPanels.Count; i++)
            {
                for (int j = 0; j < mainPanels[i].Count; j++)
                {
                    string name = "Panel-" + i + "-" + j;
                    var panels = panel_Main.Controls.Find(name, true);
                    if (panels.Count() > 0)
                    {
                        Control panel = (Control)panels[0] as Control;
                        int lx = firstX + j * width;
                        int ly = firstY + i * height;
                        panel.Location = new Point(lx, ly);
                    }
                }
            }
        }

        /// <summary>
        /// 把上一行的位置初始化
        /// </summary>
        /// <param name="rowIndex"></param>
        private void ReSetLastRowLocation(int rowIndex)
        {
            for (int i = 0; i < mainPanels[rowIndex].Count; i++)
            {
                string name = "Panel-" + rowIndex + "-" + i;
                var panels = panel_Main.Controls.Find(name, true);
                if (panels.Count() > 0)
                {
                    Control panel = (Control)panels[0] as Control;
                    int lx = i * panel_Main.Width;
                    panel.Location = new Point(lx, panel.Location.Y);
                }
            }
            CurrentPanelIndexCol = 0;
        }

        /// <summary>
        /// 定时执行动画
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_change_Tick(object sender, EventArgs e)
        {
            switch (Direction)
            {
                case 0:
                    ToLeft();
                    break;
                case 1:
                    ToUp();
                    break;
                case 2:
                    ToRight();
                    break;
                case 3:
                    ToDown();
                    break;
            }
        }

        /// <summary>
        /// 向左
        /// </summary>
        private void ToLeft()
        {
            if (CurrentPanelIndexCol >= mainPanels[CurrentPanelIndexRow].Count() - 1)
            {
                Click_Lock = false;
                return;
            }

            if (lastLength + stepLength > panel_Main.Width)
            {
                int tempStepLength = panel_Main.Width - lastLength;
                for (int i = 0; i < mainPanels[CurrentPanelIndexRow].Count; i++)
                {
                    string name = "Panel-" + CurrentPanelIndexRow + "-" + i;
                    var panels = panel_Main.Controls.Find(name, true);
                    if (panels.Count() > 0)
                    {
                        Control panel = (Control)panels[0] as Control;
                        int p1X = panel.Location.X - tempStepLength;
                        panel.Location = new Point(p1X, panel.Location.Y);
                    }
                }
                timer_change.Stop();
                CurrentPanelIndexCol += 1;
                Click_Lock = false;
            }
            else
            {
                for (int i = 0; i < mainPanels[CurrentPanelIndexRow].Count; i++)
                {
                    string name = "Panel-" + CurrentPanelIndexRow + "-" + i;
                    var panels = panel_Main.Controls.Find(name, true);
                    if (panels.Count() > 0)
                    {
                        Control panel = (Control)panels[0] as Control;
                        int p1X = panel.Location.X - stepLength;
                        panel.Location = new Point(p1X, panel.Location.Y);
                    }
                }
                lastLength += stepLength;
            }
        }

        /// <summary>
        /// 向右
        /// </summary>
        private void ToRight()
        {
            if (CurrentPanelIndexCol <= 0)
            {
                Click_Lock = false;
                return;

            }
            if (lastLength + stepLength > panel_Main.Width)
            {
                int tempStepLength = panel_Main.Width - lastLength;
                for (int i = 0; i < mainPanels[CurrentPanelIndexRow].Count; i++)
                {
                    string name = "Panel-" + CurrentPanelIndexRow + "-" + i;
                    var panels = panel_Main.Controls.Find(name, true);
                    if (panels.Count() > 0)
                    {
                        Control panel = (Control)panels[0] as Control;
                        int p1X = panel.Location.X + tempStepLength;
                        panel.Location = new Point(p1X, panel.Location.Y);
                    }
                }
                timer_change.Stop();
                CurrentPanelIndexCol -= 1;
                Click_Lock = false;
            }
            else
            {
                for (int i = 0; i < mainPanels[CurrentPanelIndexRow].Count; i++)
                {
                    string name = "Panel-" + CurrentPanelIndexRow + "-" + i;
                    var panels = panel_Main.Controls.Find(name, true);
                    if (panels.Count() > 0)
                    {
                        Control panel = (Control)panels[0] as Control;
                        int p1X = panel.Location.X + stepLength;
                        panel.Location = new Point(p1X, panel.Location.Y);
                    }
                }
                lastLength += stepLength;
            }
        }

        /// <summary>
        /// 向上
        /// </summary>
        private void ToUp()
        {
            if (CurrentPanelIndexRow >= mainPanels.Count() - 1)
            {
                Click_Lock = false;
                return;
            }

            if (lastLength + stepLength > panel_Main.Height)
            {
                int tempStepLength = panel_Main.Height - lastLength;
                for (int i = 0; i < mainPanels.Count; i++)
                {
                    for (int j = 0; j < mainPanels[i].Count; j++)
                    {
                        string name = "Panel-" + i + "-" + j;
                        var panels = panel_Main.Controls.Find(name, true);
                        if (panels.Count() > 0)
                        {
                            Control panel = (Control)panels[0] as Control;
                            int p1Y = panel.Location.Y - tempStepLength;
                            panel.Location = new Point(panel.Location.X, p1Y);
                        }
                    }

                }
                timer_change.Stop();
                ReSetLastRowLocation(CurrentPanelIndexRow);
                CurrentPanelIndexRow += 1;
                Click_Lock = false;

            }
            else
            {
                for (int i = 0; i < mainPanels.Count; i++)
                {
                    for (int j = 0; j < mainPanels[i].Count; j++)
                    {
                        string name = "Panel-" + i + "-" + j;
                        var panels = panel_Main.Controls.Find(name, true);
                        if (panels.Count() > 0)
                        {
                            Panel panel = (Panel)panels[0] as Panel;
                            int p1Y = panel.Location.Y - stepLength;
                            panel.Location = new Point(panel.Location.X, p1Y);
                        }
                    }
                }
                lastLength += stepLength;
            }
        }

        /// <summary>
        /// 向下
        /// </summary>
        private void ToDown()
        {
            if (CurrentPanelIndexRow <= 0)
            {
                Click_Lock = false;
                return;
            }

            if (lastLength + stepLength > panel_Main.Height)
            {
                int tempStepLength = panel_Main.Height - lastLength;
                for (int i = 0; i < mainPanels.Count; i++)
                {
                    for (int j = 0; j < mainPanels[i].Count; j++)
                    {
                        string name = "Panel-" + i + "-" + j;
                        var panels = panel_Main.Controls.Find(name, true);
                        if (panels.Count() > 0)
                        {
                            Control panel = (Control)panels[0] as Control;
                            int p1Y = panel.Location.Y + tempStepLength;
                            panel.Location = new Point(panel.Location.X, p1Y);
                        }
                    }

                }
                timer_change.Stop();
                ReSetLastRowLocation(CurrentPanelIndexRow);
                CurrentPanelIndexRow -= 1;
                Click_Lock = false;
            }
            else
            {
                for (int i = 0; i < mainPanels.Count; i++)
                {
                    for (int j = 0; j < mainPanels[i].Count; j++)
                    {
                        string name = "Panel-" + i + "-" + j;
                        var panels = panel_Main.Controls.Find(name, true);
                        if (panels.Count() > 0)
                        {
                            Control panel = (Control)panels[0] as Control;
                            int p1Y = panel.Location.Y + stepLength;
                            panel.Location = new Point(panel.Location.X, p1Y);
                        }
                    }
                }
                lastLength += stepLength;
            }

        }
    }
}
