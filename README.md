# CarouselForm

#### 介绍
winform 封装一个幻灯片滚动动画

#### 功能介绍
采用定时器+绘制自动以控件位置操作，并封装成工具，一句话调用即可实现动画效果。


#### 动画展示，可以扩展幻灯片播放
![Image text](https://gitee.com/zuiyuewentian/CarouselForm/raw/master/Images/loading.gif)


#### 实际项目中使用展示

![Image text](https://gitee.com/zuiyuewentian/CarouselForm/raw/master/Images/test.gif)

#### 使用方式，参考项目中：

`mainFormController.Init(5, 3, panel_Main.Width, panel_Main.Height);`



